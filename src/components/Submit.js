import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {COLOR_RED} from '../contstants';

function Submit(props) {
    return (
        <TouchableOpacity style={styles.button} activeOpacity={0.8} onPress={props.onPress}>
            <Text style={styles.buttonText}>{props.title.toUpperCase()}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    button: {
        height: 50,
        width: '100%',
        backgroundColor: COLOR_RED,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
    },
    buttonText: {
        color: '#fff',
        fontWeight: '500'
    },
});

export {Submit};