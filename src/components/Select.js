import React from 'react';
import {Picker, StyleSheet, View} from 'react-native';

function Select(props) {
    return (
        <Picker
            style={styles.select}
            selectedValue={props.selectedValue}
            onValueChange={props.onValueChange}>
            {props.options.map((item, index) => {
                return <Picker.Item label={item.label} value={item.value} key={index}/>
            })}
        </Picker>
    );
}

const styles = StyleSheet.create({
    select: {
        width: '100%',
        marginBottom: 15,
    },
});

export {Select};