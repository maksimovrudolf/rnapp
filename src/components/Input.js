import React from 'react';
import {StyleSheet, TextInput} from 'react-native';

function Input(props) {
    return (
        <TextInput style={styles.input} {...props} />
    );
}

const styles = StyleSheet.create({
    input: {
        borderColor: '#cbcbcb',
        borderWidth: 1,
        marginBottom: 15,
        paddingHorizontal: 15,
        height: 50,
        width: '100%',
        borderRadius: 4,
    },
});

export {Input};