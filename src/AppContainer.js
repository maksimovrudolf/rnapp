import React from 'react';
import {Platform} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import {
    createStackNavigator,
    createBottomTabNavigator,
    createSwitchNavigator,
    createAppContainer
} from 'react-navigation';

import {
    AuthLoadingScreen,
    SignInScreen,
    ModulesScreen,
    ModuleScreen,
    ProfileScreen,
    ProfileEditScreen
} from './screens/';

const AuthStack = createStackNavigator({
    SignIn: SignInScreen
});

const ModuleStack = createStackNavigator({
    Modules: ModulesScreen,
    Module: ModuleScreen,
});

const ProfileStack = createStackNavigator({
    Profile: ProfileScreen,
    ProfileEdit: ProfileEditScreen
});

const AppStack = createBottomTabNavigator({
    Modules: {
        screen: ModuleStack,
        navigationOptions: {
            tabBarLabel: 'Модули',
            tabBarIcon: ({focused, tintColor}) => {
                let iconName = 'list-box';

                if (Platform.OS === 'ios') {
                    iconName = 'ios-' + iconName;
                } else {
                    iconName = 'md-' + iconName;
                }

                return <Ionicons name={iconName} size={25} color={tintColor}/>;
            }
        }
    },
    Profile: {
        screen: ProfileStack,
        navigationOptions: {
            tabBarLabel: 'Профиль',
            tabBarIcon: ({focused, tintColor}) => {
                let iconName = 'person';

                if (Platform.OS === 'ios') {
                    iconName = 'ios-' + iconName;
                } else {
                    iconName = 'md-' + iconName;
                }

                return <Ionicons name={iconName} size={25} color={tintColor}/>;
            }
        }
    },
});

export default createAppContainer(createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        Auth: AuthStack,
        App: AppStack,
    },
    {
        initialRouteName: 'AuthLoading',
    }
));