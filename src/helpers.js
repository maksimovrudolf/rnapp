import {HEADERS_TYPE} from './contstants';

export function getHeadersAuth(userToken) {
    return {
        ...HEADERS_TYPE,
        'Authorization': 'Bearer ' + userToken
    };
}