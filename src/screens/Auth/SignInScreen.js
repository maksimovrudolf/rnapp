import React from 'react';
import {AsyncStorage, StyleSheet, View, Image, Alert, KeyboardAvoidingView, ScrollView, Text} from 'react-native';
import {ProgressDialog, ConfirmDialog} from 'react-native-simple-dialogs';
import {API, HEADERS_TYPE} from '../../contstants';
import {Input, Submit} from '../../components';

class SignInScreen extends React.Component {
    static navigationOptions = {
        title: 'Авторизация',
    };

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            isProgress: false,
            message: '',
        };
    }

    handleChangeInput = (name, value) => {
        this.setState({
            [name]: value
        });
    };

    render() {
        return (
            <KeyboardAvoidingView behavior='padding' style={styles.main}>
                <ScrollView contentContainerStyle={{flexGrow: 1}} keyboardShouldPersistTaps='handled'>
                    <View style={styles.container}>
                        <ProgressDialog visible={this.state.isProgress} message='Загрузка...'/>
                        <ConfirmDialog
                            title="Ошибка"
                            visible={!!this.state.message}
                            onTouchOutside={this.clearMessage}
                            positiveButton={{title: "OK", onPress: this.clearMessage}}>
                            <View>
                                <Text>{this.state.message}</Text>
                            </View>
                        </ConfirmDialog>
                        <Image style={styles.logo} source={require('../../assets/logo.png')}/>
                        <Input
                            placeholder='Email'
                            value={this.state.email}
                            onChangeText={this.handleChangeInput.bind(this, 'email')}/>
                        <Input
                            placeholder='Password'
                            value={this.state.password}
                            secureTextEntry={true}
                            onChangeText={this.handleChangeInput.bind(this, 'password')}/>
                        <Submit onPress={this.handlePress} title='Войти'/>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }

    clearMessage = () => {
        this.setState({message: ''});
    };

    handlePress = () => {
        let message = '';

        if (!this.state.email.trim()) {
            message += 'Необходимо заполнить «Email».\n';
        }

        if (!this.state.password.trim()) {
            message += 'Необходимо заполнить «Пароль».\n';
        }

        if (message) {
            Alert.alert('Ошибка', message.trim());
            return;
        }

        this.signInAsync();
    };

    signInAsync = async () => {
        try {
            this.setState({isProgress: true});

            const rawResponse = await fetch(API + '/default/login/', {
                method: 'POST',
                headers: {...HEADERS_TYPE},
                body: JSON.stringify({
                    email: this.state.email,
                    password: this.state.password
                })
            });

            const data = await rawResponse.json();
            let message = '';

            if (data.token) {
                await AsyncStorage.setItem('userToken', data.token);
                this.props.navigation.navigate('App');
            } else {
                if (Array.isArray(data)) {
                    for (let i = 0; i < data.length; i++) {
                        message += data[i].message + '\n';
                    }
                }

                this.setState({
                    isProgress: false,
                    message: message.trim()
                });
            }
        } catch (e) {
            console.log(e.message);
        }
    };
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 15,
    },
    logo: {
        width: 194,
        height: 74,
        marginBottom: 50,
    },
});

export {SignInScreen};