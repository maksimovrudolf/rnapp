import React from 'react';
import {Button, StyleSheet, View, AsyncStorage, ScrollView, Text} from 'react-native';
import {API, COLOR_RED} from '../../contstants';
import {Progress} from '../../components/Progress';
import {getHeadersAuth} from '../../helpers';

class ModuleScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        return {
            title: navigation.getParam('title', 'Загрузка...'),
        };
    };

    constructor(props) {
        super(props);

        const {navigation} = this.props;

        this.state = {
            moduleId: navigation.getParam('id', ''),
            data: {
                id: 0,
                name: '',
                materials: [],
                tests: [],
            },
            isProgress: false,
        };
    }

    render() {
        if (this.state.isProgress) {
            return <Progress/>;
        }

        return (
            <ScrollView>
                <View style={styles.container}>
                    <Text style={styles.title}>Материалы</Text>
                    <View style={styles.list}>
                        {
                            this.state.data.materials.map((item) => {
                                return <View style={styles.listItem} key={item.id}>
                                    <View style={styles.text}>
                                        <Text>{item.name}</Text>
                                    </View>
                                    <View>
                                        <Button
                                            title='Перейти'
                                            disabled={true}
                                            color={COLOR_RED}
                                            onPress={this.openMaterial.bind(null, item.id, this.state.moduleId)}/>
                                    </View>
                                </View>;
                            })
                        }
                    </View>
                </View>
            </ScrollView>
        );
    }

    openMaterial = (id, moduleId) => {
        console.log(id, moduleId);
    };

    componentDidMount = () => {
        this.loadModule();
    };

    loadModule = async () => {
        try {
            this.setState({isProgress: true});

            const userToken = await AsyncStorage.getItem('userToken');
            const headers = getHeadersAuth(userToken);

            const rawResponse = await fetch(API + '/module/view/?id=' + this.state.moduleId, {
                method: 'GET',
                headers: {...headers}
            });

            const data = await rawResponse.json();

            this.setState({
                data: {
                    id: data.id,
                    name: data.name,
                    materials: data.materials.map((item) => {
                        return {...item};
                    }),
                    tests: data.materials.map((item) => {
                        return {...item};
                    }),
                },
                isProgress: false
            });
        } catch (e) {
            console.log(e.message);
        }
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 30,
        paddingHorizontal: 15,
    },
    title: {
        fontSize: 24,
        fontWeight: '700',
        marginBottom: 20,
        width: '100%',
        textAlign: 'left'
    },
    list: {
        width: '100%',
    },
    listItem: {
        width: '100%',
        marginBottom: 15,
        flexDirection: 'row',
    },
    text: {
        marginRight: 15,
        flex: 1
    },
});

export {ModuleScreen};