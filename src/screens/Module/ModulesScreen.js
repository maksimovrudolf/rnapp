import React from 'react';
import {Button, StyleSheet, View, AsyncStorage, Text, ScrollView} from 'react-native';
import {API, COLOR_RED} from '../../contstants';
import {Progress} from '../../components/Progress';
import {getHeadersAuth} from '../../helpers';

class ModulesScreen extends React.Component {
    static navigationOptions = {
        title: 'Модули',
    };

    constructor(props) {
        super(props);

        this.state = {
            data: {
                modules: [],
                message: '',
            },
            isProgress: false,
        };
    }

    render() {
        if (this.state.isProgress) {
            return <Progress/>;
        }

        return (
            <ScrollView>
                <View style={styles.container}>
                    <View style={styles.list}>
                        {
                            this.state.data.modules.map((item) => {
                                return <View style={styles.listItem} key={item.id}>
                                    <View style={styles.text}>
                                        <Text>{item.name}</Text>
                                        {item.message ? <Text>{item.message}</Text> : null}
                                    </View>
                                    <View>
                                        <Button
                                            title='Перейти'
                                            disabled={!item.isOpen}
                                            color={COLOR_RED}
                                            onPress={this.openModule.bind(null, item.id, item.name)}/>
                                    </View>
                                </View>;
                            })
                        }
                    </View>
                    {this.showMessage()}
                </View>
            </ScrollView>
        );
    }

    showMessage() {
        if (this.state.data.message) {
            return <View style={styles.message}>
                <Text>{this.state.data.message}</Text>
            </View>;
        } else {
            return null;
        }
    }

    openModule = (id, title) => {
        this.props.navigation.navigate('Module', {
            id,
            title
        });
    };

    componentDidMount = () => {
        this.loadModules();
    };

    loadModules = async () => {
        try {
            this.setState({isProgress: true});

            const userToken = await AsyncStorage.getItem('userToken');
            const headers = getHeadersAuth(userToken);

            const rawResponse = await fetch(API + '/module/', {
                method: 'GET',
                headers: {...headers}
            });

            const data = await rawResponse.json();

            this.setState({
                data: {
                    modules: data.modules.map((item) => {
                        return {...item};
                    }),
                    message: data.message
                },
                isProgress: false
            });
        } catch (e) {
            console.log(e.message);
        }
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 30,
        paddingHorizontal: 15,
    },
    list: {
        width: '100%',
    },
    listItem: {
        width: '100%',
        marginBottom: 15,
        flexDirection: 'row',
    },
    text: {
        marginRight: 15,
        flex: 1
    },
    message: {
        width: '100%',
        marginTop: 10
    }
});

export {ModulesScreen};