import React from 'react';
import {StyleSheet, View, KeyboardAvoidingView, ScrollView, AsyncStorage, Alert} from 'react-native';
import ProgressDialog from 'react-native-simple-dialogs/src/ProgressDialog';
import {API} from '../../contstants';
import {Input, Select, Submit} from '../../components';
import {getHeadersAuth} from '../../helpers';

class ProfileEditScreen extends React.Component {
    static navigationOptions = {
        title: 'Редактирование профиля',
    };

    constructor(props) {
        super(props);

        const {navigation} = this.props;
        const regionsObj = navigation.getParam('regions', {});

        let regions = [{value: '', label: 'Регион'}];
        let i = 1;

        for (let key in regionsObj) {
            if (regionsObj.hasOwnProperty(key)) {
                regions[i] = {value: key, label: regionsObj[key]};
                i++;
            }
        }

        this.state = {
            surname: navigation.getParam('surname', ''),
            name: navigation.getParam('name', ''),
            patronymic: navigation.getParam('patronymic', ''),
            regionId: navigation.getParam('regionId', '') + '',
            regions: regions,
            isProgress: false,
        };
    }

    handleChangeInput = (name, value) => {
        this.setState({
            [name]: value
        });
    };

    render() {
        return (
            <KeyboardAvoidingView behavior='padding' style={styles.main}>
                <ScrollView contentContainerStyle={{flexGrow: 1}} keyboardShouldPersistTaps='handled'>
                    <View style={styles.container}>
                        <ProgressDialog visible={this.state.isProgress} message='Загрузка...'/>
                        <Input
                            placeholder='Фамилия'
                            value={this.state.surname}
                            onChangeText={this.handleChangeInput.bind(this, 'surname')}/>
                        <Input
                            placeholder='Имя'
                            value={this.state.name}
                            onChangeText={this.handleChangeInput.bind(this, 'name')}/>
                        <Input
                            placeholder='Отчество'
                            value={this.state.patronymic}
                            onChangeText={this.handleChangeInput.bind(this, 'patronymic')}/>
                        <Select
                            selectedValue={this.state.regionId}
                            onValueChange={this.handleChangeInput.bind(this, 'regionId')}
                            options={this.state.regions}/>
                        <Submit onPress={this.handlePress} title='Сохранить'/>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }

    handlePress = () => {
        const {navigation} = this.props;

        if (
            this.state.surname === navigation.getParam('surname', '') &&
            this.state.name === navigation.getParam('name', '') &&
            this.state.patronymic === navigation.getParam('patronymic', '') &&
            this.state.regionId === navigation.getParam('regionId', '') + ''
        ) {
            navigation.goBack();
        } else {
            this.saveFields();
        }
    };

    saveFields = async () => {
        try {
            this.setState({isProgress: true});

            const userToken = await AsyncStorage.getItem('userToken');
            const headers = getHeadersAuth(userToken);

            const rawResponse = await fetch(API + '/profile/edit/', {
                method: 'POST',
                headers: {...headers},
                body: JSON.stringify({
                    surname: this.state.surname,
                    name: this.state.name,
                    patronymic: this.state.patronymic,
                    region: this.state.regionId
                })
            });

            const data = await rawResponse.json();
            this.setState({isProgress: false});

            if (data === 'success') {
                this.props.navigation.navigate('Profile', {
                    refresh: true
                });
            } else {
                let message = '';

                if (Array.isArray(data)) {
                    for (let i = 0; i < data.length; i++) {
                        message += data[i].message + '\n';
                    }
                }

                message = message.trim();

                Alert.alert('Ошибка', message);
            }
        } catch (e) {
            console.log(e.message);
        }
    };
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 30,
        paddingHorizontal: 15,
    },
});

export {ProfileEditScreen};