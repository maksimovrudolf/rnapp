import React from 'react';
import {AsyncStorage, Button, StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import {NavigationEvents} from 'react-navigation';
import {API, COLOR_RED} from '../../contstants';
import {Progress} from '../../components/Progress';
import {getHeadersAuth} from '../../helpers';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    block: {
        marginBottom: 30,
        alignItems: 'center',
    },
    label: {
        marginBottom: 10,
    },
    span: {
        fontWeight: 'bold',
    },
    logout: {
        height: 40,
        justifyContent: 'center',
        paddingHorizontal: 15,
    }
});

class ProfileScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        return {
            title: 'Профиль',
            headerRight: (
                <TouchableOpacity
                    style={styles.logout}
                    onPress={async () => {
                        await AsyncStorage.clear();
                        navigation.navigate('Auth');
                    }}
                >
                    <Text>Выйти</Text>
                </TouchableOpacity>
            ),
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            data: {
                surname: '',
                name: '',
                patronymic: '',
                regionId: '',
                regions: {},
            },
            isProgress: false,
        };
    }

    render() {
        if (this.state.isProgress) {
            return <Progress/>;
        }

        return (
            <View style={styles.container}>
                <NavigationEvents onWillFocus={this.handleWillFocus}/>
                <View style={styles.block}>
                    <Text style={styles.label}>
                        <Text style={styles.span}>Фамилия: </Text>
                        <Text>{this.state.data.surname}</Text>
                    </Text>
                    <Text style={styles.label}>
                        <Text style={styles.span}>Имя: </Text>
                        <Text>{this.state.data.name}</Text>
                    </Text>
                    <Text style={styles.label}>
                        <Text style={styles.span}>Отчество: </Text>
                        <Text>{this.state.data.patronymic}</Text>
                    </Text>
                    <Text style={styles.label}>
                        <Text style={styles.span}>Регион: </Text>
                        <Text>{this.state.data.regions[this.state.data.regionId]}</Text>
                    </Text>
                </View>
                <Button title='Редактировать' onPress={this.showEditProfile} color={COLOR_RED}/>
            </View>
        );
    }

    showEditProfile = () => {
        this.props.navigation.navigate('ProfileEdit', {
            ...this.state.data
        });
    };

    componentDidMount = () => {
        this.loadProfile();
    };

    handleWillFocus = (payload) => {
        const {navigation} = this.props;

        if (navigation.getParam('refresh', false)) {
            this.loadProfile();
        }
    };

    loadProfile = async () => {
        try {
            this.setState({isProgress: true});

            const userToken = await AsyncStorage.getItem('userToken');
            const headers = getHeadersAuth(userToken);

            const rawResponse = await fetch(API + '/profile/', {
                method: 'GET',
                headers: {...headers}
            });

            const data = await rawResponse.json();

            this.setState({
                data: {
                    surname: data.surname,
                    name: data.name,
                    patronymic: data.patronymic,
                    regionId: data.regionId,
                    regions: {...data.regions}
                }
            });

            this.setState({isProgress: false});
        } catch (e) {
            console.log(e.message);
        }
    };
}

export {ProfileScreen};